## Software repositroy
[F-Droid](https://f-droid.org/)

## Security
### 2FA
[Aegis](https://getaegis.app/)

### Advertisments and trackers blocking
[Duckduckgo](https://spreadprivacy.com/introducing-app-tracking-protection/)

### Password manager
* [KeePassDX](https://www.keepassdx.com/)
* [Bitwarden](https://bitwarden.com/)

### Tor
[Orbot](https://guardianproject.info/apps/org.torproject.android/)

### Firewall
[NetGuard](https://netguard.me/)

## QR code scanner
[QR Scanner](https://f-droid.org/en/packages/com.secuso.privacyFriendlyCodeScanner/)  
[Github link](https://github.com/SecUSo/privacy-friendly-qr-scanner)

## Keyboard
[AnySoftKeyboard](https://anysoftkeyboard.github.io/)

## Youtube client
[NewPipe](https://newpipe.net/)

## File managers
* [Total Commander](https://www.ghisler.com/android.htm)
* [FX File Explorer](http://www.nextapp.com/fx/)

## Terminal
[Termux](https://termux.com/)

## Camera
[Open Camera](https://f-droid.org/en/packages/net.sourceforge.opencamera/)

## Chat
* [Telegram](https://telegram.org/)
* [Signal](https://www.signal.org/)

## Mathematics
* [Photomath](https://photomath.com/)
* [HiPER Scientific Calculator](https://play.google.com/store/apps/details?id=cz.hipercalc&hl=en_US&gl=US)

## Web browser
* [Firefox](https://www.mozilla.org/en-US/firefox/browsers/mobile/android/)
* [Vivaldi](https://vivaldi.com/android/)

## Audio/video player
[VLC](https://www.videolan.org/vlc/download-android.html)

## Public transport (Czech)
* [CG Transit](https://www.circlegate.com/)
* [NaVlak](https://www.circlegate.com/cs/navlak)
* [DPMBinfo](https://play.google.com/store/apps/details?id=cz.dpmb.dpmbinfo)

## Translator
[DIC-o](https://www.dic-o.com/)

## Multimedia
### Drawing and photo annotating
[Draw](https://www.simplemobiletools.com/) (Simple Mobile Tools)

### Gallery
[Simple Gallery](https://www.simplemobiletools.com/) (Simple Mobile Tools)

## Navigation
[Waze](https://www.waze.com/)

## PDF viewers
### Secure PDF
[GitHub link](https://github.com/GrapheneOS/PdfViewer)  
[Google Play link](https://play.google.com/store/apps/details?id=app.grapheneos.pdfviewer.play)
